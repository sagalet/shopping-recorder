import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shopping_recorder/l10n/messages_all.dart';

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'zh-TW'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return AppLocalizations.load(locale);
  }

  @override
  bool shouldReload(covariant LocalizationsDelegate<AppLocalizations> old) =>
      false;
}

class AppLocalizations {
  static Future<AppLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new AppLocalizations();
    });
  }

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  String get categories {
    return Intl.message("categories");
  }

  String get filters {
    return Intl.message("filters");
  }

  String get priceLowToHigh {
    return Intl.message("price_low_to_high");
  }

  String get priceHighToLow {
    return Intl.message("price_high_to_low");
  }

  String get dateOldest {
    return Intl.message("date_oldest");
  }

  String get dateLatest {
    return Intl.message("date_latest");
  }

  String get titleListPage {
    return Intl.message("list_page");
  }

  String get category {
    return Intl.message("category");
  }

  String get addNewCommodity {
    return Intl.message("add_new_commodity");
  }

  String get all {
    return Intl.message("all");
  }

  String get sort {
    return Intl.message("sort");
  }

  String get addCategory {
    return Intl.message("add_category");
  }

  String get add {
    return Intl.message("add");
  }

  String get update {
    return Intl.message("update");
  }

  String get cancel {
    return Intl.message("cancel");
  }

  String get modify {
    return Intl.message("modify");
  }

  String get alreadyExists {
    return Intl.message("already_exists");
  }

  String get ok {
    return Intl.message("ok");
  }

  String get newCommodity {
    return Intl.message("new_commodity");
  }

  String get name {
    return Intl.message("name");
  }

  String get place {
    return Intl.message("place");
  }

  String get date {
    return Intl.message("date");
  }

  String get time {
    return Intl.message("time");
  }

  String get price {
    return Intl.message("price");
  }

  String get description {
    return Intl.message("description");
  }
}
