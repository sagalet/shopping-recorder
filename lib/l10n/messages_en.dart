// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "add" : MessageLookupByLibrary.simpleMessage("Add"),
    "add_category" : MessageLookupByLibrary.simpleMessage("Add category"),
    "add_new_commodity" : MessageLookupByLibrary.simpleMessage("Add new commodity"),
    "all" : MessageLookupByLibrary.simpleMessage("All"),
    "already_exists" : MessageLookupByLibrary.simpleMessage("already exists"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "categories" : MessageLookupByLibrary.simpleMessage("Categories"),
    "category" : MessageLookupByLibrary.simpleMessage("Category"),
    "date" : MessageLookupByLibrary.simpleMessage("Date"),
    "date_latest" : MessageLookupByLibrary.simpleMessage("Latest"),
    "date_oldest" : MessageLookupByLibrary.simpleMessage("Oldest"),
    "description" : MessageLookupByLibrary.simpleMessage("Description"),
    "filters" : MessageLookupByLibrary.simpleMessage("Filters"),
    "list_page" : MessageLookupByLibrary.simpleMessage("List"),
    "modify" : MessageLookupByLibrary.simpleMessage("Modify"),
    "name" : MessageLookupByLibrary.simpleMessage("Name"),
    "new_commodity" : MessageLookupByLibrary.simpleMessage("New commodity"),
    "ok" : MessageLookupByLibrary.simpleMessage("Ok"),
    "place" : MessageLookupByLibrary.simpleMessage("Place"),
    "price" : MessageLookupByLibrary.simpleMessage("Price"),
    "price_high_to_low" : MessageLookupByLibrary.simpleMessage("Price: high to low"),
    "price_low_to_high" : MessageLookupByLibrary.simpleMessage("Price: low to high"),
    "sort" : MessageLookupByLibrary.simpleMessage("Sort"),
    "time" : MessageLookupByLibrary.simpleMessage("Time"),
    "update" : MessageLookupByLibrary.simpleMessage("Update")
  };
}
