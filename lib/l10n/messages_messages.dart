// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'messages';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "add" : MessageLookupByLibrary.simpleMessage("add"),
    "add_category" : MessageLookupByLibrary.simpleMessage("add_category"),
    "add_new_commodity" : MessageLookupByLibrary.simpleMessage("add_new_commodity"),
    "all" : MessageLookupByLibrary.simpleMessage("all"),
    "already_exists" : MessageLookupByLibrary.simpleMessage("already_exists"),
    "cancel" : MessageLookupByLibrary.simpleMessage("cancel"),
    "categories" : MessageLookupByLibrary.simpleMessage("categories"),
    "category" : MessageLookupByLibrary.simpleMessage("category"),
    "date" : MessageLookupByLibrary.simpleMessage("date"),
    "date_latest" : MessageLookupByLibrary.simpleMessage("date_latest"),
    "date_oldest" : MessageLookupByLibrary.simpleMessage("date_oldest"),
    "description" : MessageLookupByLibrary.simpleMessage("description"),
    "filters" : MessageLookupByLibrary.simpleMessage("filters"),
    "list_page" : MessageLookupByLibrary.simpleMessage("list_page"),
    "modify" : MessageLookupByLibrary.simpleMessage("modify"),
    "name" : MessageLookupByLibrary.simpleMessage("name"),
    "new_commodity" : MessageLookupByLibrary.simpleMessage("new_commodity"),
    "ok" : MessageLookupByLibrary.simpleMessage("ok"),
    "place" : MessageLookupByLibrary.simpleMessage("place"),
    "price" : MessageLookupByLibrary.simpleMessage("price"),
    "price_high_to_low" : MessageLookupByLibrary.simpleMessage("price_high_to_low"),
    "price_low_to_high" : MessageLookupByLibrary.simpleMessage("price_low_to_high"),
    "sort" : MessageLookupByLibrary.simpleMessage("sort"),
    "time" : MessageLookupByLibrary.simpleMessage("time"),
    "update" : MessageLookupByLibrary.simpleMessage("update")
  };
}
