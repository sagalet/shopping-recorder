import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shopping_recorder/database/local_database.dart';
import 'package:shopping_recorder/locale/app_localizations.dart';

class CategoryPage extends StatefulWidget {
  final LocalDataBase database;

  const CategoryPage({Key key, @required this.database}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  
  List<Category> _categories = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.database.getCategories().then((value) => {
          if (value.length > 0)
            setState(() {
              _categories = value;
            })
        });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(title: Text(AppLocalizations.of(context).category)),
        body: Builder(
          builder: (context) => Column(
            children: [
              Padding(
                  padding: EdgeInsets.all(5.0),
                  child: FlatButton(
                    child: Row(children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
                          child: Icon(Icons.add_circle_outline_rounded,
                              color: Colors.green)),
                      Text(AppLocalizations.of(context).addCategory)
                    ]),
                    onPressed: () {
                      _showCategoryDialog(
                              null,
                              AppLocalizations.of(context).add,
                              AppLocalizations.of(context).cancel)
                          .then((value) => {
                                if (value != null)
                                  {
                                    if (_containCategory(value))
                                      {
                                        Scaffold.of(context)
                                            .showSnackBar(SnackBar(
                                          backgroundColor: Colors.red,
                                          content: Text("\"$value\" " +
                                              AppLocalizations.of(context)
                                                  .alreadyExists),
                                        ))
                                      }
                                    else
                                      {
                                        widget.database
                                            .addCategory(value)
                                            .then((id) => {
                                                  setState(() {
                                                    _categories.add(
                                                        Category(id, value));
                                                  })
                                                })
                                            .catchError((error) {
                                          Scaffold.of(context)
                                              .showSnackBar(SnackBar(
                                            content: Text(error.toString()),
                                          ));
                                        })
                                      }
                                  }
                              });
                    },
                  )),
              if (_categories != null && _categories.length > 0)
                Flexible(
                    child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return ListTile(
                        leading: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            IconButton(
                              icon: Icon(Icons.remove_circle_outline,
                                  color: Colors.red),
                              onPressed: () {
                                final cate = _categories.elementAt(index);
                                widget.database
                                    .deleteCategory(cate.id)
                                    .then((value) => setState(() {
                                          _categories.removeAt(index);
                                        }));
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.edit, color: Colors.blue),
                              onPressed: () {
                                _showCategoryDialog(
                                        _categories.elementAt(index).name,
                                        AppLocalizations.of(context).ok,
                                        AppLocalizations.of(context).cancel)
                                    .then((value) => {
                                          if (value !=
                                              _categories.elementAt(index).name)
                                            {
                                              widget.database
                                                  .updateCategory(
                                                      _categories
                                                          .elementAt(index)
                                                          .id,
                                                      value)
                                                  .then((id) => {
                                                        setState(() {
                                                          _categories
                                                              .removeAt(index);
                                                          _categories.insert(
                                                              index,
                                                              Category(
                                                                  id, value));
                                                        })
                                                      })
                                            }
                                        });
                              },
                            ),
                          ],
                        ),
                        title: Text(_categories.elementAt(index).name));
                  },
                  itemCount: _categories.length,
                )),
            ],
          ),
        ));
  }

  Future<String> _showCategoryDialog(
      String text, String ok, String cancel) async {
    TextEditingController controller = TextEditingController(text: text);
    return await showDialog<String>(
      context: context,
      child: AlertDialog(
        contentPadding: const EdgeInsets.all(16.0),
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextField(
                controller: controller,
                autofocus: true,
                decoration: new InputDecoration(
                    labelText: AppLocalizations.of(context).category),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              child: Text(ok),
              onPressed: () {
                Navigator.pop(context, controller.text);
              }),
          new FlatButton(
              child: Text(cancel),
              onPressed: () {
                Navigator.pop(context, null);
              })
        ],
      ),
    );
  }

  bool _containCategory(String name) {
    for (Category c in _categories) {
      if (c.name == name) return true;
    }
    return false;
  }
}
