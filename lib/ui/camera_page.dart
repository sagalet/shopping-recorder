import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

class CameraPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    // 3

    _getCamera().then((value) {
      _controller = CameraController(value, ResolutionPreset.medium);
      _initializeControllerFuture = _controller.initialize();
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          // 4
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(this._controller);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      // 5
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.camera), onPressed: _takePicture),
    );
  }

  // 6
  void _takePicture() async {
    try {
      await _initializeControllerFuture;

      final xfile = await _controller.takePicture();
      Navigator.pop(context, xfile);
    } catch (e) {
      print(e);
    }
  }

  // 7
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<CameraDescription> _getCamera() async {
    final camerasList = await availableCameras();
    return camerasList.first;
  }
}
