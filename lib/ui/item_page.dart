import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:shopping_recorder/database/local_database.dart';
import 'package:shopping_recorder/locale/app_localizations.dart';
import 'package:shopping_recorder/ui/widget/Item_detail_widget.dart';
import 'package:shopping_recorder/ui/widget/camera_widget.dart';
import 'package:shopping_recorder/Constants.dart';

// ignore: must_be_immutable
class ItemPage extends StatefulWidget {
  ItemStatus status = ItemStatus.SHOW;
  final LocalDataBase database;

  ItemPage({Key key, this.status, @required this.database}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ItemPageState();
}

class _ItemPageState extends State<ItemPage> {
  @override
  Widget build(BuildContext context) {
    final edge =
        MediaQuery.of(context).size.height > MediaQuery.of(context).size.width
            ? MediaQuery.of(context).size.width
            : MediaQuery.of(context).size.height;

    final cameraWidget = CameraWidget(edge: edge, status: widget.status);
    final itemDetail = ItemDetail(
        edge: edge, status: widget.status, database: widget.database);

    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).newCommodity),
      ),
      body: ListView(
        children: [
          Center(
            child: cameraWidget,
          ),
          itemDetail,
          if (widget.status == ItemStatus.NEW)
            Center(
              child: FlatButton(
                child: Text(
                  AppLocalizations.of(context).add,
                  style: TextStyle(color: Colors.blue),
                ),
                onPressed: (){
                  Uint8List image = cameraWidget.getData();
                  ItemDetailData data = itemDetail.getData();
                  Map<String, dynamic> item = data.toMap();
                  item["photo"] = image;
                  widget.database.addCommodity(item);
                },
              ),
            ),
          if (widget.status == ItemStatus.EDIT)
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  FlatButton(
                    child: Text(
                      AppLocalizations.of(context).add,
                      style: TextStyle(color: Colors.blue),
                    ),
                  ),
                  FlatButton(
                    child: Text(
                      AppLocalizations.of(context).add,
                      style: TextStyle(color: Colors.blue),
                    ),
                  ),
                ],
              ),
            ),
          if (widget.status == ItemStatus.SHOW)
            Center(
              child: FlatButton(
                child: Text(
                  AppLocalizations.of(context).add,
                  style: TextStyle(color: Colors.blue),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
