import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shopping_recorder/Constants.dart';
import 'package:shopping_recorder/database/local_database.dart';
import 'package:shopping_recorder/locale/app_localizations.dart';
import 'package:shopping_recorder/ui/category_page.dart';
import 'package:shopping_recorder/ui/item_page.dart';
import 'package:shopping_recorder/ui/widget/category_picker.dart';

enum _SortType { PRICE_LTH, PRICE_HTL, OLDEST, LATEST }

class ListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  int _category = -1;
  _SortType _sortType = _SortType.PRICE_LTH;
  LocalDataBase _database;

  @override
  void initState() {
    super.initState();
    _database = LocalDataBase.getDataBase();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(AppLocalizations.of(context).titleListPage)),
      drawer: Drawer(
        child: Builder(
          builder: (context) => ListView(
            children: [
              DrawerHeader(child: null),
              ListTile(
                leading: Icon(Icons.category),
                title: Text(AppLocalizations.of(context).category),
                onTap: () {
                  Scaffold.of(context).openEndDrawer();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CategoryPage(
                                database: _database,
                              ))).then((_) => {setState(() {})});
                },
              ),
              ListTile(
                leading: Icon(Icons.add),
                title: Text(AppLocalizations.of(context).addNewCommodity),
                onTap: () {
                  Scaffold.of(context).openEndDrawer();
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return ItemPage(
                      status: ItemStatus.NEW,
                      database: _database,
                    );
                  })).then((_) => {});
                },
              )
            ],
          ),
        ),
      ),
      body: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Row(
                  children: [
                    Flexible(
                        child: Padding(
                            padding: EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
                            child: Text(AppLocalizations.of(context).category)),
                        flex: 1),
                    Expanded(
                        child: CategoryPicker(
                          database: _database,
                          showAll: true,
                          onChange: (value) => setState(() {
                            _category = value;
                          }),
                        ),
                        flex: 1),
                  ],
                ),
                flex: 1,
              ),
              Expanded(
                child: Row(
                  children: [
                    Flexible(
                        child: Padding(
                            padding: EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
                            child: Text(AppLocalizations.of(context).sort)),
                        flex: 1),
                    Expanded(
                        child: DropdownButton(
                          items: _getSortType(),
                          onChanged: (value) {
                            setState(() {
                              _sortType = value;
                            });
                          },
                          value: _sortType,
                        ),
                        flex: 1),
                  ],
                ),
                flex: 1,
              ),
            ],
          ),
        ],
      ),
    );
  }

  List<DropdownMenuItem> _getSortType() {
    List<DropdownMenuItem> items = List();
    items.add(DropdownMenuItem(
        child: Text(AppLocalizations.of(context).priceLowToHigh),
        value: _SortType.PRICE_LTH));
    items.add(DropdownMenuItem(
        child: Text(AppLocalizations.of(context).priceHighToLow),
        value: _SortType.PRICE_HTL));
    items.add(DropdownMenuItem(
        child: Text(AppLocalizations.of(context).dateOldest),
        value: _SortType.OLDEST));
    items.add(DropdownMenuItem(
        child: Text(AppLocalizations.of(context).dateLatest),
        value: _SortType.LATEST));
    return items;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _database.closeDB();
  }
}
