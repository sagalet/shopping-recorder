import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:shopping_recorder/database/local_database.dart';
import 'package:shopping_recorder/Constants.dart';
import 'package:shopping_recorder/ui/camera_page.dart';

class CameraWidget extends StatefulWidget {
  final double edge;
  _CameraWidgetState _state;
  ItemStatus status = ItemStatus.SHOW;

  CameraWidget({Key key, @required this.edge, int id, this.status})
      : super(key: key) {
    _state = _CameraWidgetState(id);
  }

  @override
  State<StatefulWidget> createState() {
    return _state;
  }

  void setState(ItemStatus status) {
    this.status = status;
    _state.update();
  }

  Uint8List getData() => _state.getData();
}

class _CameraWidgetState extends State<CameraWidget> {
  Uint8List _image;

  _CameraWidgetState(int id) {
    LocalDataBase database = LocalDataBase.getDataBase();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: GestureDetector(
            child: _buildCamera(),
            onTap: () {
              print("tap camera");
              if (widget.status == ItemStatus.EDIT) {
                Navigator.push<XFile>(context,
                        MaterialPageRoute(builder: (context) => CameraPage()))
                    .then((value) => {
                          if (value != null)
                            {
                              value.readAsBytes().then((image) => setState(() {
                                    _image = image;
                                  }))
                            }
                        });
              }
            }));
  }

  Widget _buildCamera() {
    return Container(
      width: widget.edge,
      height: widget.edge * 3 / 4,
      child: LayoutBuilder(
        builder: (context, constraints) {
          final bound = constraints.maxWidth > constraints.maxHeight
              ? constraints.maxHeight
              : constraints.maxWidth;
          return (_image == null || _image.length == 0)
              ? Icon(
                  Icons.add_a_photo,
                  color: Colors.grey,
                  size: bound / 3,
                )
              : Image.memory(_image);
        },
      ),
    );
  }

  void update() => setState(() {});

  Uint8List getData() {
    return _image;
  }
}
