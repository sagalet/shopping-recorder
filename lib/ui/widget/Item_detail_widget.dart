import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_place/google_place.dart';
import 'package:intl/intl.dart';
import 'package:shopping_recorder/database/local_database.dart';
import 'package:shopping_recorder/locale/app_localizations.dart';
import 'package:shopping_recorder/ui/widget/google_place_widget.dart';
import 'package:shopping_recorder/Constants.dart';

import 'category_picker.dart';

// ignore: must_be_immutable
class ItemDetail extends StatefulWidget {
  final LocalDataBase database;

  final double edge;

  _ItemDetailState _state;

  ItemDetail(
      {Key key,
      ItemStatus status,
      @required this.edge,
      ItemDetailData data,
      @required this.database})
      : super(key: key) {
    _state = _ItemDetailState(data: data, status: status);
  }

  @override
  State<StatefulWidget> createState() {
    return _state;
  }

  void setState(ItemStatus status) {
    _state.update(status);
  }

  ItemDetailData getData() {
    return _state.getData();
  }
}

class _ItemDetailState extends State<ItemDetail> {
  ItemStatus status = ItemStatus.SHOW;

  final TextEditingController _dateController = TextEditingController();
  final TextEditingController _timeController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  GooglePlaceField _googlePlaceField;
  int _category;
  CategoryPicker _categoryPicker;

  _ItemDetailState({ItemDetailData data, this.status}) {
    if (data != null) {
      _nameController.text = data.name;
      _priceController.text = data.price.toString();
      _dateController.text = data.date;
      _timeController.text = data.time;
      _descriptionController.text = data.description;
      _category = data.category;
    }
  }

  @override
  Widget build(BuildContext context) {
    _googlePlaceField = GooglePlaceField(status: status);
    _categoryPicker = CategoryPicker(
      database: widget.database,
      status: status,
      category: _category,
      showAll: false,
    );
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.edge / 10),
      child: Column(
        children: [
          TextField(
            enabled: status != ItemStatus.SHOW,
            controller: _nameController,
            decoration: InputDecoration(
                icon: Icon(Icons.title),
                labelText: AppLocalizations.of(context).name),
          ),
          TextField(
            enabled: status != ItemStatus.SHOW,
            controller: _priceController,
            inputFormatters: [
              _UsNumberTextInputFormatter(),
            ],
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              icon: Icon(Icons.attach_money),
              labelText: AppLocalizations.of(context).price,
            ),
          ),
          _googlePlaceField,
          GestureDetector(
            child: AbsorbPointer(
              child: TextField(
                controller: _dateController,
                decoration: InputDecoration(
                    icon: Icon(Icons.date_range_sharp),
                    labelText: AppLocalizations.of(context).date),
              ),
            ),
            onTap: () {
              if (status != ItemStatus.SHOW) {
                showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.fromMillisecondsSinceEpoch(0),
                        lastDate: DateTime(DateTime.now().year + 1))
                    .then((value) => {
                          if (value != null)
                            _dateController.text =
                                DateFormat("yyyy/MM/dd").format(value)
                        });
              }
            },
          ),
          GestureDetector(
            child: AbsorbPointer(
              child: TextField(
                controller: _timeController,
                decoration: InputDecoration(
                  icon: Icon(Icons.access_time_sharp),
                  labelText: AppLocalizations.of(context).time,
                ),
              ),
            ),
            onTap: () {
              if (status != ItemStatus.SHOW) {
                showTimePicker(context: context, initialTime: TimeOfDay.now())
                    .then((value) => {
                          if (value != null)
                            _timeController.text =
                                DefaultMaterialLocalizations().formatTimeOfDay(
                                    value,
                                    alwaysUse24HourFormat: true)
                        });
              }
            },
          ),
          SizedBox(
            height: 60,
            child: Row(
              children: [
                Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 5.0, 15.0, 5.0),
                    child: Icon(
                      Icons.category,
                      color: Colors.black38,
                    )),
                Expanded(child: _categoryPicker, flex: 1),
              ],
            ),
          ),
          TextField(
            enabled: status != ItemStatus.SHOW,
            controller: _descriptionController,
            maxLines: 3,
            maxLengthEnforced: true,
            maxLength: 150,
            decoration: InputDecoration(
                icon: Icon(Icons.description),
                labelText: AppLocalizations.of(context).description),
          ),
        ],
      ),
    );
  }

  void update(ItemStatus status) {
    this.status = status;
    setState(() {});
  }

  ItemDetailData getData() {
    return ItemDetailData(
        _nameController.text,
        _googlePlaceField.getData(),
        double.parse(_priceController.text),
        _dateController.text,
        _timeController.text,
        _category,
        _descriptionController.text);
  }
}

class _UsNumberTextInputFormatter extends TextInputFormatter {
  static const defaultDouble = 0.001;

  static double strToFloat(String str, [double defaultValue = defaultDouble]) {
    try {
      return double.parse(str);
    } catch (e) {
      return defaultValue;
    }
  }

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    String value = newValue.text;
    int selectionIndex = newValue.selection.end;
    if (value == ".") {
      value = "0.";
      selectionIndex++;
    } else if (value != "" &&
        value != defaultDouble.toString() &&
        strToFloat(value, defaultDouble) == defaultDouble) {
      value = oldValue.text;
      selectionIndex = oldValue.selection.end;
    }
    return new TextEditingValue(
      text: value,
      selection: new TextSelection.collapsed(offset: selectionIndex),
    );
  }
}

class ItemDetailData {
  final String name;
  final AutocompletePrediction location;
  final double price;
  final String date;
  final String time;
  final int category;
  final String description;

  ItemDetailData(this.name, this.location, this.price, this.date, this.time,
      this.category, this.description);

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map();
    map["name"] = name;
    map["location"] = location.description;
    map["price"] = price;
    map["time"] = "$date $time";
    map["category"] = category;
    map["description"] = description;
    return map;
  }

  static ItemDetailData fromMap(Map<String, dynamic> map) {
    String tmp = map["time"];
    String date;
    String time;
    if (tmp != null) {
      List<String> lists = tmp.trim().split(" ");
      if (lists.length >= 2) {
        date = lists[0];
        time = lists[1];
      }
    }
    return ItemDetailData(map["name"], map["location"], map["price"], date,
        time, map["category"], map["description"]);
  }
}
