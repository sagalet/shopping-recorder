import 'package:flutter/material.dart';
import 'package:shopping_recorder/Constants.dart';
import 'package:shopping_recorder/database/local_database.dart';
import 'package:shopping_recorder/locale/app_localizations.dart';

class CategoryPicker extends StatefulWidget {
  final void Function(int) onChange;
  final LocalDataBase database;

  _CategoryPickerState _state;

  CategoryPicker(
      {Key key,
      this.onChange,
      @required this.database,
      ItemStatus status,
      int category,
      bool showAll})
      : super(key: key) {
    _state = _CategoryPickerState(
        category: category, status: status, showAll: showAll);
  }

  @override
  State<StatefulWidget> createState() => _state;

  int getData() {
    return _state.getDate();
  }
}

class _CategoryPickerState extends State<CategoryPicker> {
  int category = -1;
  List<Category> _categories = List();
  ItemStatus status = ItemStatus.SHOW;
  bool showAll = false;

  _CategoryPickerState({this.category, this.status, this.showAll});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _loadCategory();
    return DropdownButton(
      items: _getCategories(),
      onChanged: (value) => {
        setState(() {
          print("$value select");
          category = value;
          if (widget.onChange != null) widget.onChange(value);
        })
      },
      value: category,
    );
  }

  List<DropdownMenuItem> _getCategories() {
    List<DropdownMenuItem> items = List();
    if (showAll) {
      items.add(DropdownMenuItem(
          child: Text(AppLocalizations.of(context).all), value: -1));
    }
    if (_categories != null && _categories.length > 0) {
      _categories.forEach((element) {
        items.add(
            DropdownMenuItem(child: Text(element.name), value: element.id));
      });
    }
    return items;
  }

  void _loadCategory() {
    if (widget.database != null) {
      widget.database.getCategories().then((value) {
        if (value.length > 0) {
          setState(() {
            _categories = value;
          });
        }
      });
    }
  }

  int getDate() {
    return category;
  }
}
