import 'package:flutter/material.dart';
import 'package:google_place/google_place.dart';
import 'package:shopping_recorder/Constants.dart';
import 'package:shopping_recorder/locale/app_localizations.dart';

class GooglePlaceField extends StatefulWidget {
  _GooglePlaceFieldState _state;

  GooglePlaceField(
      {Key key, ItemStatus status, AutocompletePrediction prediction})
      : super(key: key) {
    _state =
        _GooglePlaceFieldState(status: status, selectedPrediction: prediction);
  }

  @override
  State<StatefulWidget> createState() {
    return _state;
  }

  AutocompletePrediction getData() {
    return _state.getData();
  }

  void setState(ItemStatus status) {
    _state.update(status);
  }
}

class _GooglePlaceFieldState extends State<GooglePlaceField> {
  ItemStatus status = ItemStatus.SHOW;
  final _addressController = TextEditingController();
  GooglePlace _googlePlace;
  OverlayEntry _entry;
  List<AutocompletePrediction> _predictions;
  final LayerLink _layerLink = LayerLink();

  final FocusNode _focusNode = FocusNode();

  AutocompletePrediction selectedPrediction;

  _GooglePlaceFieldState({this.status, this.selectedPrediction});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _googlePlace = GooglePlace(PLACE_API_KEY);
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        if (_addressController.text.isNotEmpty) {
          _autoComplete();
        }
      } else {
        _removeAutoCompletedList();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return CompositedTransformTarget(
        link: _layerLink,
        child: TextField(
          focusNode: _focusNode,
          enabled: status != ItemStatus.SHOW,
          controller: _addressController,
          onChanged: (value) => {
            if (value.isNotEmpty)
              _autoComplete()
            else
              _removeAutoCompletedList()
          },
          onSubmitted: (value) => Focus.of(context).unfocus(),
          decoration: InputDecoration(
              icon: Icon(Icons.place),
              labelText: AppLocalizations.of(context).place),
        ));
  }

  OverlayEntry _createOverlayEntry() {
    RenderBox renderBox = context.findRenderObject();
    var size = renderBox.size;
    var offset = renderBox.localToGlobal(Offset.zero);
    return OverlayEntry(
      builder: (context) => Positioned(
          width: size.width,
          height: size.height * 3,
          child: CompositedTransformFollower(
            link: _layerLink,
            showWhenUnlinked: false,
            offset: Offset(0.0, size.height + 5.0),
            child: Material(
              elevation: 4.0,
              child: ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.red,
                      child: Icon(
                        Icons.pin_drop,
                        color: Colors.white,
                      ),
                    ),
                    title:
                        Text(_predictions[index].structuredFormatting.mainText),
                    subtitle: Text(
                        _predictions[index].structuredFormatting.secondaryText),
                    onTap: () {
                      selectedPrediction = _predictions[index];
                      _addressController.text =
                          selectedPrediction.structuredFormatting.mainText;
                      _removeAutoCompletedList();
                      _focusNode.unfocus();
                    },
                  );
                },
                itemCount: _predictions.length,
              ),
            ),
          )),
    );
  }

  void _autoComplete() {
    _googlePlace.autocomplete.get(_addressController.text).then((value) {
      if (_addressController.text.isNotEmpty && _focusNode.hasFocus) {
        _predictions = value.predictions;
        _updateAutoCompletedList();
      }
    });
  }

  void _updateAutoCompletedList() {
    OverlayState state = Overlay.of(context);
    if (_entry == null) {
      _entry = _createOverlayEntry();
      state.insert(_entry);
    } else {
      _entry.markNeedsBuild();
    }
  }

  void _removeAutoCompletedList() {
    if (_entry != null) {
      _entry.remove();
      _entry = null;
      _predictions = [];
    }
  }

  void update(ItemStatus status) {
    this.status = status;
    setState(() {});
  }

  AutocompletePrediction getData() {
    if (selectedPrediction == null) {
      selectedPrediction = AutocompletePrediction(
          reference: _addressController.text,
          structuredFormatting:
              StructuredFormatting(mainText: _addressController.text));
    }
    return selectedPrediction;
  }
}
