import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:shopping_recorder/Constants.dart';

class LocalDataBase {
  Database _database;

  static LocalDataBase _instance;

  static LocalDataBase getDataBase() {
    if (_instance == null) {
      _instance = LocalDataBase._();
    }
    return _instance;
  }

  LocalDataBase._();

  Future<Database> _init() async {
    Database database = await openDatabase(
        join(await getDatabasesPath(), DATABASE_NAME), onCreate: (db, version) {
      db.execute(
          "CREATE TABLE $CATEGORY_DB (id INTEGER PRIMARY KEY AUTOINCREMENT,"
          "name TEXT NOT NULL UNIQUE);");
      // create location db??
      return db.execute("CREATE TABLE $COMMODITY_DB ("
          "id INTEGER PRIMARY KEY AUTOINCREMENT,"
          "name TEXT NOT NULL,"
          "price REAL NOT NULL,"
          "location TEXT,"
          "time UNSIGNED BIG INT,"
          "category INTEGER,"
          "description TEXT,"
          "photo BLOB,"
          "FOREIGN KEY(category) REFERENCES categories(id));");
    }, version: 1);
    return database;
  }

  Future<Database> _getConnectDB() async {
    if (_database == null) {
      _database = await _init();
    }
    return _database;
  }

  void closeDB() {
    if (_database != null) {
      _database.close();
      _database = null;
    }
  }

  Future<List<Category>> getCategories() async {
    Database db = await _getConnectDB();
    List<Map<String, dynamic>> lists = await db.query(CATEGORY_DB);
    List<Category> categories = List();
    if (lists != null && lists.length > 0) {
      lists.forEach((element) {
        categories.add(Category.fromMap(element));
      });
    }
    return categories;
  }

  void getCommodityByCategory(int category) {}

  Future<int> addCategory(String category) async {
    Database db = await _getConnectDB();
    Map<String, String> value = Map();
    value["name"] = category;
    return db.insert(CATEGORY_DB, value);
  }

  Future<int> deleteCategory(int id) {
    return _database.delete(CATEGORY_DB, where: "id=?", whereArgs: [id]);
  }

  Future<int> updateCategory(int id, String name) {
    return _database.update(CATEGORY_DB, Category(id, name).toMap(),
        where: "id = ?", whereArgs: <int>[id]);
  }

  Future<List<Commodity>> getCommodity(int id) async {
    Database db = await _getConnectDB();
    List<Map<String, dynamic>> lists =
        await db.query(COMMODITY_DB, where: "id=?", whereArgs: [id]);
    List<Commodity> commodities = List();
    if (lists != null && lists.length > 0) {
      lists.forEach((element) {
        commodities.add(Commodity.fromMap(element));
      });
    }
    return commodities;
  }

  Future<int> addCommodity(Map<String, dynamic> commodity) {
    return _database.insert(COMMODITY_DB, commodity);
  }

  Future<int> deleteCommodity(int id) {
    return _database.delete(COMMODITY_DB, where: "id = ?", whereArgs: [id]);
  }

  Future<int> deleteAllCommodities(int category) {
    return _database
        .delete(COMMODITY_DB, where: "category = ?", whereArgs: [category]);
  }
}

class Category {
  final int id;
  final String name;

  Category(this.id, this.name);

  static Category fromMap(Map<String, dynamic> item) {
    if (item["id"] == null || item["name"] == null) {
      return null;
    }
    return Category(item["id"], item["name"]);
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> item = Map();
    item["id"] = id;
    item["name"] = name;
    return item;
  }
}

class Commodity {
  final int id;
  final String name;
  final double price;
  final String location;
  final String time;
  final String description;
  final ByteData photo;
  final int category;

  Commodity(this.id, this.name, this.price, this.location, this.time,
      this.category, this.description, this.photo);

  static Commodity fromMap(Map<String, dynamic> item) {
    return Commodity(item["id"], item["name"], item["price"], item["location"],
        item["time"], item["category"], item["description"], item["photo"]);
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> item = Map();
    item["id"] = id;
    item["name"] = name;
    item["price"] = price;
    item["location"] = location;
    item["category"] = category;
    item["description"] = description;
    item["photo"] = photo;
    return item;
  }
}
